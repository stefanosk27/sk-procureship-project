package com.procureship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkProcureshipProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkProcureshipProjectApplication.class, args);
	}
}
